﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pg.Siri.EchoUI
{
    public partial class FrmSetting : Form
    {
        private readonly Main _main;

        public FrmSetting(Main main)
        {
            InitializeComponent();
            _main = main;

            this.txtContent.Text = _main.ReadTxtContent();
        }

        private void btnSaveClose_Click(object sender, EventArgs e)
        {
            _main.WriteTxtContent(txtContent.Text);
            this.Close();
        }
    }
}
