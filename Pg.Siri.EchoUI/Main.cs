﻿using SiriPlugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pg.Siri.EchoUI
{
    public class Main : IPlugin
    {
        public string Id => "Pg.Siri.EchoUI";

        public string Version => "1.0";

        public string Name => "EchoUI";

        public string Author => "邓振振";

        public string Description => "带UI配置的样例,演示了如何获取当前插件的目录，如何配置窗口等";

        public void Execute()
        {
            var txtContent = ReadTxtContent();
            System.Windows.Forms.MessageBox.Show("嘿嘿，" + txtContent);
        }

        public void Initialize()
        {
            var txtFullName = Path.Combine(CurrentDirectory, "echo.txt");
            if (!File.Exists(txtFullName))
                File.WriteAllText(txtFullName, "我是调用Initialize生成的哦", Encoding.UTF8);
        }

        public bool SettingUI() => true;

        public void ShowSettingUI()
        {
            new FrmSetting(this).ShowDialog();
        }

        /// <summary>
        /// 当前插件的目录
        /// </summary>
        public string CurrentDirectory => Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Plugins", Id);

        public string ReadTxtContent()
        {
            var txtFullName = Path.Combine(CurrentDirectory, "echo.txt");
            var txtContent = "";
            if (File.Exists(txtFullName))
            {
                txtContent = File.ReadAllText(txtFullName, Encoding.UTF8);
            }
            return txtContent;
        }

        public void WriteTxtContent(string content)
        {
            var txtFullName = Path.Combine(CurrentDirectory, "echo.txt");
            File.WriteAllText(txtFullName, content, Encoding.UTF8);
        }
    }
}
