﻿using SiriPlugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiriRobot
{
    public class App
    {
        /// <summary>
        /// 插件目录
        /// </summary>
        public static string PluginDirectoryPath => FileHelper.Combine(AppDomain.CurrentDomain.BaseDirectory, "Plugins");
    }
}
