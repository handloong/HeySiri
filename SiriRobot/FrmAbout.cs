﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SiriRobot
{
    public partial class FrmAbout : Form
    {
        public FrmAbout()
        {
            InitializeComponent();


            txtVersion.Text = $"版本:V{Assembly.GetExecutingAssembly().GetName().Version}";
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("https://gitee.com/handloong/HeySiri.git");
            }
            catch {}
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnThanks_Click(object sender, EventArgs e)
        {
            MessageBox.Show(@"本程序使用了以下第三方类库:
MQTTnet [https://github.com/dotnet/MQTTnet]", @"感谢", MessageBoxButtons.OK,MessageBoxIcon.Information);
        }
    }
}
