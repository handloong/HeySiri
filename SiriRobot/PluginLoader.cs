﻿using SiriPlugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using static System.Net.WebRequestMethods;

namespace SiriRobot
{
    // 创建插件加载器
    public class PluginLoader
    {
        public static List<IPlugin> Plugins = new List<IPlugin>();

        public static void LoadPlugins(string directoryPath)
        {
            if (string.IsNullOrWhiteSpace(directoryPath))
            {
                throw new ArgumentException($"“{nameof(directoryPath)}”不能为 null 或空白。", nameof(directoryPath));
            }
            // 加载插件目录中的所有插件
            var pluginFiles = Directory.GetFiles(directoryPath, "*.dll", SearchOption.AllDirectories);

            foreach (string file in pluginFiles)
            {
                try
                {
                    byte[] assemblyData = System.IO.File.ReadAllBytes(file);
                    Assembly assembly = Assembly.Load(assemblyData);
                    Type[] types = assembly.GetTypes();

                    foreach (Type type in types)
                    {
                        if (typeof(IPlugin).IsAssignableFrom(type))
                        {
                            IPlugin existingPlugin = Plugins.FirstOrDefault(p => p.GetType() == type);

                            // 如果插件已加载，则进行更新操作
                            if (existingPlugin != null)
                                Plugins.Remove(existingPlugin);

                            IPlugin plugin = (IPlugin)Activator.CreateInstance(type);
                            try
                            {
                                plugin.Initialize();
                                Plugins.Add(plugin);
                            }
                            catch
                            {
                                //Initialize 失败不加载插件
                            }
                        }
                    }
                }
                catch{}
            }

            Plugins = Plugins.OrderBy(x=>x.Id).ToList();
        }

        public static void ReLoadPlugins(string directoryPath)
        {
            UnloadAll();
            LoadPlugins(directoryPath);
        }

        public static bool ExecutePlugin(string pluginId)
        {
            if (string.IsNullOrWhiteSpace(pluginId))
                throw new ArgumentException($"“{nameof(pluginId)}”不能为 null 或空白。", nameof(pluginId));

            IPlugin plugin = Plugins.FirstOrDefault(p => p.Id == pluginId);

            return ExecutePlugin(plugin);
        }

        public static bool ExecutePlugin(IPlugin plugin)
        {
            if (plugin != null)
            {
                plugin.Execute();
                return true;
            }

            return false;
        }

        public static bool UnloadPlugin(IPlugin plugin)
        {
            if (plugin != null)
            {
                Plugins.Remove(plugin);
                return true;
            }

            return false;
        }

        public static bool UnloadPlugin(string pluginId)
        {
            if (string.IsNullOrWhiteSpace(pluginId))
            {
                throw new ArgumentException($"“{nameof(pluginId)}”不能为 null 或空白。", nameof(pluginId));
            }

            IPlugin plugin = Plugins.FirstOrDefault(p => p.Id == pluginId);

            return UnloadPlugin(plugin);
        }

        public static void UnloadAll()
        {
            Plugins.Clear();
        }
    }
}
