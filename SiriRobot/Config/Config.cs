﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiriRobot
{
    public class Config
    {
        /// <summary>
        /// 842F14C86D2D45238C539366BD05D57D
        /// topic = $"T_{c}";
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// 47.103.148.154
        /// </summary>
        public string MQTTServer { get; set; } = "47.103.148.154";

        /// <summary>
        /// 1883
        /// </summary>
        public int MQTTPort { get; set; } = 1883;

        /// <summary>
        /// anonymous
        /// </summary>
        public string UserName { get; set; } = "anonymous";

        /// <summary>
        /// anonymous
        /// </summary>
        public string Password { get; set; } = "anonymous";

        /// <summary>
        /// Siri要访问的地址
        /// </summary>
        public string Url { get; set; } = "http://47.103.148.154:8000";

        /// <summary>
        /// 开机自启
        /// </summary>
        public bool AutoRun { get; set; } = true;
    }
}
