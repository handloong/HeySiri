﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SiriRobot
{
    /// <summary>
    /// 文件帮助类
    /// </summary>
    public class FileHelper
    {
        #region 获取文件MD5值
        /// <summary>
        /// 获取文件MD5值
        /// </summary>
        /// <param name="fileName">文件绝对路径</param>
        /// <returns>MD5值</returns>
        public static string GetMD5HashFromFile(string fileName)
        {
            MD5 md5 = MD5.Create();
            string s = string.Empty;
            using (FileStream fs = File.OpenRead(fileName))
            {
                byte[] b = md5.ComputeHash(fs);
                for (int i = 0; i < b.Length; i++)
                {
                    s += b[i].ToString("x2");
                }
            }
            md5.Clear();
            return s;
        }
        #endregion

        #region 获取指定文件夹下的所有文件
        /// <summary>
        /// 获取指定文件夹下的所有文件,包括里面的嵌套文件夹
        /// </summary>
        /// <param name="directory">文件夹不存在</param>
        /// <param name="searchFilePattern">文件匹配符 多个使用|分割,例如 *.dat|*.txt 默认*.*</param>
        public static List<FileInfo> GetDirectoryAllFiles(string directory,
            string searchFilePattern = "*.*")
        {
            if (Directory.Exists(directory))
            {
                var files = searchFilePattern
                    .Split('|')
                    .SelectMany(sp => Directory.EnumerateFiles(directory, sp, SearchOption.AllDirectories));

                return files.Select(x => new FileInfo(x)).ToList();

                //不使用递归

                //fileFullNames.AddRange(files);

                //var directories = searchFolderPattern
                //     .Split('|')
                //     .SelectMany(sp => Directory.EnumerateDirectories(directory, sp));

                //foreach (var dir in directories)
                //{
                //    fileFullNames.AddRange(GetDirectoryAllFiles(dir, searchFilePattern, searchFolderPattern));
                //}
            }
            return new List<FileInfo>();
        }
        #endregion

        #region CreateDirectory
        /// <summary>
        /// 如果目录不存在则创建一个目录
        /// </summary>
        /// <param name="path"></param>
        public static void CreateDirectory(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }
        /// <summary>
        /// 如果目录不存在则创建一个目录
        /// </summary>
        /// <param name="paths"></param>
        public static void CreateDirectory(string[] paths)
        {
            var full = Path.Combine(paths);
            if (!Directory.Exists(full))
            {
                CreateDirectory(full);
            }
        }
        #endregion

        #region Path.Combine 并自动创建Combine后的文件夹
        /// <summary>
        /// Path.Combine 并自动创建Combine后的文件夹
        /// </summary>
        /// <param name="path1"></param>
        /// <param name="path2"></param>
        /// <returns></returns>
        public static string Combine(string path1, string path2)
        {
            var ret = Path.Combine(path1, path2);
            CreateDirectory(ret);
            return ret;
        }
        /// <summary>
        /// Path.Combine 并自动创建Combine后的文件夹
        /// </summary>
        /// <param name="path1"></param>
        /// <param name="path2"></param>
        /// <param name="path3"></param>
        /// <returns></returns>
        public static string Combine(string path1, string path2, string path3)
        {
            var ret = Path.Combine(path1, path2, path3);
            CreateDirectory(ret);
            return ret;
        }
        /// <summary>
        /// Path.Combine 并自动创建Combine后的文件夹
        /// </summary>
        /// <param name="path1"></param>
        /// <param name="path2"></param>
        /// <param name="path3"></param>
        /// <param name="path4"></param>
        /// <returns></returns>
        public static string Combine(string path1, string path2, string path3, string path4)
        {
            var ret = Path.Combine(path1, path2, path3, path4);
            CreateDirectory(ret);
            return ret;
        }
        /// <summary>
        /// Path.Combine 并自动创建Combine后的文件夹
        /// </summary>
        /// <param name="paths"></param>
        /// <returns></returns>
        public static string Combine(params string[] paths)
        {
            var ret = Path.Combine(paths);
            CreateDirectory(ret);
            return ret;
        }
        #endregion

        #region GetFiles
        /// <summary>
        /// 获取路径下的所有文件,如果目录不存在返回空集合(不是null)
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static List<FileInfo> GetFiles(string path)
        {
            if (!Directory.Exists(path))
            {
                return new List<FileInfo>();
            }
            return new DirectoryInfo(path).GetFiles().ToList();
        }
        #endregion
    }
}
