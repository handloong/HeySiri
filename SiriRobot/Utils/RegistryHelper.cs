﻿using Microsoft.Win32;
using System;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Button;

namespace SiriRobot
{
    public class RegistryHelper
    {
        public static void AutoRun(bool autoRun)
        {
            var appName = "Siri Robot";
            try
            {
                RegistryKey localKey = Registry.LocalMachine;
                RegistryKey runKey = localKey.CreateSubKey(@"software\microsoft\windows\currentversion\run");
                if (autoRun)
                    runKey.SetValue(appName, Application.ExecutablePath);
                else
                    runKey.DeleteValue(appName, false);

                runKey.Close();
                localKey.Close();
            }
            catch { }
        }

        public static bool GetAutoRun()
        {
            var appName = "Siri Robot";
            try
            {
                RegistryKey localKey = Registry.LocalMachine;
                RegistryKey runKey = localKey.CreateSubKey(@"software\microsoft\windows\currentversion\run");

                runKey.Close();
                localKey.Close();
                return runKey.GetValue(appName) != null;
            }
            catch { }
            return false;
        }
    }
}
