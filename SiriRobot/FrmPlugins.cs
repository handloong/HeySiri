﻿using SiriPlugin;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace SiriRobot
{
    public partial class FrmPlugins : Form
    {
        public FrmPlugins()
        {
            InitializeComponent();
            InitializeListView();
            _txtUrlSize = txtUrl.Size;
        }

        Size _txtUrlSize;
        private void InitializeListView()
        {
            listView.Columns.Add("插件", 180, HorizontalAlignment.Left);
            listView.Columns.Add("版本", 80, HorizontalAlignment.Left);
            listView.Columns.Add("作者", 80, HorizontalAlignment.Left);

            listView.View = View.Details;
            listView.HideSelection = false;
            listView.FullRowSelect = true;
        }

        private void FrmPlugins_Load(object sender, EventArgs e)
        {
            foreach (var item in PluginLoader.Plugins)
            {
                ListViewItem lvItem = new ListViewItem(new[] { item.Name, item.Version, item.Author });
                lvItem.Tag = item;
                listView.Items.Add(lvItem);
            }
        }

        private void FrmPlugins_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void listView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (listView.SelectedItems.Count > 0)
            {
                ListViewItem selectedItem = listView.SelectedItems[0];
                var plugin = selectedItem.Tag as IPlugin; // 获取选中行的Tag值


                lblID.Text = plugin.Id;
                lblName.Text = plugin.Name;
                lblVersion.Text = plugin.Version;
                lblAuthor.Text = plugin.Author;
                txtDescription.Text = plugin.Description;

                var config = EasyConf.Read<Config>();
                txtUrl.Text = $"{config.Url}/hey?p={plugin.Id}&c={config.ClientId}";

                var hasUI = plugin.SettingUI();
                btnShowSettingUI.Visible = hasUI;
                if (!hasUI)
                    txtUrl.Size = new Size { Width = _txtUrlSize.Width, Height = _txtUrlSize.Height + 30 };
                else
                    txtUrl.Size = _txtUrlSize;

                btnShowSettingUI.Click -= BtnShowSettingUI_Click;
                btnShowSettingUI.Tag = plugin;
                btnShowSettingUI.Click += BtnShowSettingUI_Click;
            }
        }

        private void BtnShowSettingUI_Click(object sender, EventArgs e)
        {
            var plugin = (sender as Button).Tag as IPlugin;
            try
            {
                plugin.ShowSettingUI();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lblID_Click(object sender, EventArgs e)
        {
            try
            {
                var dir = Path.Combine(App.PluginDirectoryPath, lblID.Text);
                Process.Start(dir);
            }
            catch { }
        }

        //private void btnRemovePlugin_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        //{
        //    if (listView.SelectedItems.Count > 0)
        //    {
        //        ListViewItem selectedItem = listView.SelectedItems[0];
        //        var plugin = selectedItem.Tag as IPlugin; // 获取选中行的Tag值
        //        PluginLoader.UnloadPlugin(plugin);
        //        //PluginLoader.ReLoadPlugins(App.PluginDirectoryPath);
        //    }
        //}
    }
}
