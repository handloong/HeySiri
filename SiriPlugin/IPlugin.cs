﻿using System;

namespace SiriPlugin
{
    public interface IPlugin
    {
        /// <summary>
        /// 插件ID,格式: pg.xxx.xx  例如:pg.siri.shutdown
        /// </summary>
        string Id { get; }

        /// <summary>
        /// 版本
        /// </summary>
        string Version { get; }

        /// <summary>
        /// 插件名称,例如 Siri关机插件
        /// </summary>
        string Name { get; }

        /// <summary>
        /// 插件作者
        /// </summary>
        string Author { get; }

        /// <summary>
        /// 描述
        /// </summary>
        string Description { get; }

        /// <summary>
        /// 初始化插件
        /// </summary>
        void Initialize();

        /// <summary>
        /// 执行插件
        /// </summary>
        void Execute();

        /// <summary>
        /// 是否有设置页面,如果有返回true
        /// </summary>
        /// <returns></returns>
        bool SettingUI();

        /// <summary>
        /// 显示该插件的设置页面
        /// </summary>
        void ShowSettingUI();
    }
}
