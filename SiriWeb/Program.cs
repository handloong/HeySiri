using MQTTnet;
using MQTTnet.Client;
using SiriWeb;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddHostedService<MqttWorker>();

var app = builder.Build();

// Configure the HTTP request pipeline.


app.MapGet("/status", () =>
{
    return $"Running,Client Count:{App.MqttServer.GetClientsAsync().Result.Count}";
});


//p : 要运行的插件ID
//c : 客户端唯一标识
app.MapGet("/hey", async (string p, string c) =>
{
    var topic = $"T_{c}";

    var mqttFactory = new MqttFactory();

    using (var mqttClient = mqttFactory.CreateMqttClient())
    {
        var mqttClientOptions = new MqttClientOptionsBuilder()
            .WithTcpServer("127.0.0.1")
            .WithCredentials("anonymous", "anonymous")
            .Build();

        await mqttClient.ConnectAsync(mqttClientOptions, CancellationToken.None);

        var applicationMessage = new MqttApplicationMessageBuilder()
            .WithTopic(topic)
            .WithPayload(p)
            .Build();

        await mqttClient.PublishAsync(applicationMessage, CancellationToken.None);
        await mqttClient.DisconnectAsync();

        return "sent";
    }
});

app.Run();
