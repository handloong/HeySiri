## 💐 序言

> 无私奉献不是天方夜谭，有时候，我们也可以做到。


# HeySiri

#### 介绍
Hey Siri
通过Siri远程调用插件,例如实现 HeySiri 关机

本软件MIT协议,包括所用依赖都是MIT协议.

#### 软件架构
Siri Robot 使用winform做UI，

SiriWeb通过MQTTNet类库实现MQTT服务器和web接口，通过MQTT协议实现Robot和网页接口的互通


#### 使用说明
软件主页面

![软件主页面](https://foruda.gitee.com/images/1690546609202886121/37ca8041_1514386.png "屏幕截图")


配置页面

![配置页面](https://foruda.gitee.com/images/1690546638259337061/2a2aa074_1514386.png "屏幕截图")

47.103.148.154这个是我阿里云的小水管，随便连接。

如果自己部署则需要把SiriWeb放到自己的服务器上

这个链接可以查看当前客户端连接的数量
http://47.103.148.154:8000/status

插件配置

![插件配置](https://foruda.gitee.com/images/1690546788356688897/971037e7_1514386.png "屏幕截图")

把这个URL copy下来添加siri快捷指令，获取网页内容。然后hey siri ， 关闭次卧电脑。就会执行对应的插件的Execute()的方法

![输入图片说明](Image/image.png)

插件开发

随便建个项目继承IPlugin实现相关方法就可以可参考Plugins下的Pg.Siri.Reboot

![输入图片说明](https://foruda.gitee.com/images/1690546817733957665/2c7649be_1514386.png "屏幕截图")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

